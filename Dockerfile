# docker buildx build --platform linux/amd64,linux/arm64 --push -t container_tag --build-arg CODE_SERVER_IMAGE_ARG=codercom/code-server --build-arg CODE_SERVER_VERSION_ARG=latest --build-arg GOLANG_VERSION_ARG=1.18.4  .

# https://github.com/coder/code-server
# https://github.com/coder/code-server/blob/main/ci/release-image/Dockerfile
ARG CODE_SERVER_IMAGE_ARG "codercom/code-server"
ARG CODE_SERVER_VERSION_ARG "latest"
FROM --platform=$BUILDPLATFORM ${CODE_SERVER_IMAGE_ARG}:${CODE_SERVER_VERSION_ARG}
ENV CODE_SERVER_VERSION ${CODE_SERVER_VERSION_ARG}

LABEL maintainer="docker@notmy.cloud"
LABEL org.label-schema.schema-version="2.0.0"
LABEL org.label-schema.name="notmycloud/vsgo"
LABEL org.label-schema.description="Golang with VS Code-Server inside an OCI Container"
LABEL org.label-schema.url="https://gitlab.com/notmycloud/vsgo"
LABEL org.label-schema.vcs-url="https://gitlab.com/notmycloud/vsgo"
LABEL org.label-schema.vendor="Not My Cloud"

# Reset to root user instead of coder from base image
USER root
ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8
ENV DEBIAN_FRONTEND noninteractive
ENV WORKSPACE /workspace

# Modify the Code-Server entrypoint script to allow for workspace setup scripts.
ENV ENTRYPOINTD /entrypoint.d
##RUN NEWCODE='# Allow users to have scripts run on container startup to prepare workspace.\nif [ -d \"${ENTRYPOINTD}\" ]; then\n  find "${ENTRYPOINTD}" -type f -executable -print -exec {} \\;\nfi\n' && \
##    sudo sed --in-place "/^exec.*/i${NEWCODE}" /usr/bin/entrypoint.sh

RUN apt-get update && \
    apt-get --no-install-recommends install -y \
        g++ \
        gcc \
        git \
        libc6-dev \
        make \
        pkg-config \
        curl \
        wget \
        shellcheck \
    && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get clean

ENV GOPATH /go
ENV GOROOT /usr/local/go
ENV PATH "${GOPATH}/bin:${GOROOT}/bin:${PATH}"

RUN mkdir -p "${WORKSPACE}" && \
    chown 1000:1000 "${WORKSPACE}" && \
    mkdir -p "${GOPATH}" "$GOPATH/src" "$GOPATH/bin" && \
    chown -R 1000:1000 "${GOPATH}" && \
    mkdir -p "${GOROOT}" && \
    echo 'export PATH="${GOPATH}/bin:${PATH}"' > /etc/profile.d/00-go-path.sh && \
    echo 'export PATH="${GOROOT}/bin:${PATH}"' > /etc/profile.d/00-go-root.sh
    
ARG GOLANG_VERSION_ARG "1.18.4"
ENV GOLANG_VERSION ${GOLANG_VERSION_ARG}
ARG TARGETARCH "amd64"
ENV GOARCH ${TARGETARCH}
ENV GOOS linux
RUN echo "Downloading: https://go.dev/dl/go${GOLANG_VERSION}.${GOOS}-${GOARCH}.tar.gz" && \
    curl -L https://go.dev/dl/go${GOLANG_VERSION}.${GOOS}-${GOARCH}.tar.gz | tar --strip-components=1 -C "${GOROOT}" -xz

COPY resources/* /usr/lib/code-server/src/browser/media/

# This way, if someone sets $DOCKER_USER, docker-exec will still work as
# the uid will remain the same. note: only relevant if -u isn't passed to
# docker-run.
USER 1000

VOLUME ${GOPATH}
WORKDIR ${WORKSPACE}

LABEL org.label-schema.build-date=${BUILD_DATE}
LABEL org.label-schema.vcs-ref=${VCS_REF}
LABEL org.label-schema.docker.cmd="docker run --rm -it -v $(pwd):${WORKSPACE} notmycloud/vsgo:${CODE_SERVER_VERSION}-${GOLANG_VERSION}"

