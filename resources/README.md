# Golang Favicons

These icons are useful to differentiate between this Golang Code-Server and other Code-Servers you may have running.

The source icon is provided by [Simple Icons](https://simpleicons.org/?q=go)

## ImageMagick

You will need to install `inkscape` in addition to `imagemagick`.

Use these commands to generate the PNG and ICO files from the source SVG.

```
convert -density 256x256 -background transparent golang.svg -define icon:auto-resize -colors 256 pwa-icon.png
convert -density 192x192 -background transparent golang.svg -define icon:auto-resize -colors 256 pwa-icon-192.png
convert -density 512x512 -background transparent golang.svg -define icon:auto-resize -colors 256 pwa-icon-512.png
convert -density 256x256 -background transparent golang.svg -define icon:auto-resize -colors 256 favicon.ico
cp golang.svg favicon.svg
cp favicon.svg favicon-dark-support.svg
```

