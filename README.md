# VSGO - Work in progress.

This image combines VS Code-Server and Golang so you can easily write and compile your code in a web browser.

Feel free to open issues at the parent repo: https://gitlab.com/notmycloud/vsgo/

Docker Image is based on the codercom/code-server, please reference their documentation for initial setup.
https://github.com/coder/code-server

## Setup

### Entrypoint

Mount your workspace setup scripts to `/entrypoint.d/*`.  
These will be executed as part of the container entrypoint as the `coder` user.  
Any executable file in this directory will be ran prior to starting the code-server.
You can change the entrypoint folder with the `ENTRYPOINTD` Environment variable.  
This may be useful for install PIP dependancies or code-server extensions.

### Golang

Mount your golang workspace to `/workspace`.

### VSCode

VSCode configuration is saved to `/home/coder/.local/share/code-server`.  
VSCode extensions can be installed by creating an entrypoint script as defined above with the following command.
`code-server --install-extension <extension id>`  
For example: `code-server --install-extension shan.code-settings-sync`  

I recommend using this extension to sync your settings, at least until code-server supports [native sync](https://github.com/coder/code-server/issues/2195). This extension will save your settings and extensions to a Github Gist.  
You must access VSGO from an HTTPS session for setup to work. When you get the callback of `localhost:54321/callback?code=SecretCode`, curl it into a terminal window in VSCode.  
`curl localhost:54321/callback?code=SecretCode`  

You can setup your terminal environment by placing scripts in `/etc/profile.d/`.  
These will be executed every time you open a new terminal in VSCode.

### Code Server

Code-server configuration is saved to `/home/coder/.config/code-server/config.yaml`.  
You can pass command line arguments to your Docker Run command to be passed on to the code-server instance.
```
code-server 4.3.0 09bc30027a7fbba170f907a527eaa9f7219fe739 with Code 1.65.2

Usage: code-server [options] [path]
    - Opening a directory: code-server ./path/to/your/project
    - Opening a saved workspace: code-server ./path/to/your/project.code-workspace

Options
      --auth                   The type of authentication to use. [password, none]
      --password               The password for password authentication (can only be passed in via $PASSWORD or the config file).
      --hashed-password        The password hashed with argon2 for password authentication (can only be passed in via $HASHED_PASSWORD or the config file).
                               Takes precedence over 'password'.
      --cert                   Path to certificate. A self signed certificate is generated if none is provided.
      --cert-host              Hostname to use when generating a self signed certificate.
      --cert-key               Path to certificate key when using non-generated cert.
      --disable-telemetry      Disable telemetry.
      --disable-update-check   Disable update check. Without this flag, code-server checks every 6 hours against the latest github release and
                               then notifies you once every week that a new release is available.
      --disable-file-downloads Disable file downloads from Code.
   -h --help                   Show this output.
      --open                   Open in browser on startup. Does not work remotely.
      --bind-addr              Address to bind to in host:port. You can also use $PORT to override the port.
      --config                 Path to yaml config file. Every flag maps directly to a key in the config file.
      --socket                 Path to a socket (bind-addr will be ignored).
      --socket-mode            File mode of the socket.
   -v --version                Display version information.
      --user-data-dir          Path to the user data directory.
      --extensions-dir         Path to the extensions directory.
      --list-extensions        List installed VS Code extensions.
      --force                  Avoid prompts when installing VS Code extensions.
      --install-extension      Install or update a VS Code extension by id or vsix. The identifier of an extension is `${publisher}.${name}`.
                               To install a specific version provide `@${version}`. For example: 'vscode.csharp@1.2.3'.
      --enable-proposed-api    Enable proposed API features for extensions. Can receive one or more extension IDs to enable individually.
      --uninstall-extension    Uninstall a VS Code extension by id.
      --show-versions          Show VS Code extension versions.
      --github-auth            GitHub authentication token (can only be passed in via $GITHUB_TOKEN or the config file).
      --proxy-domain           Domain used for proxying ports.
   -e --ignore-last-opened     Ignore the last opened directory or workspace in favor of an empty window.
   -n --new-window             Force to open a new window.
   -r --reuse-window           Force to open a file or folder in an already opened window.
 -vvv --verbose                Enable verbose logging.
      --link                   (deprecated) Securely bind code-server via our cloud service with the passed name. You'll get a URL like
                               https://hostname-username.coder.co at which you can easily access your code-server instance.
                               Authorization is done via GitHub.
```

### Docker/Podman

Basic usage: `docker run --rm -it -v $(pwd):/workspace notmycloud/vsgo:${CODE_SERVER_VERSION}-${GOLANG_VERSION}`  
For example, the following command will run a v4 code-server with v1.18.<LATEST> of Golang.
`docker run --rm -it -v $(pwd):/workspace notmycloud/vsgo:v4-v118`

For the GOLANG section of the tag, v118 would have the latest build of Golang 1.18.X, whereas v1180 would be Golang 1.18, just the base release.
